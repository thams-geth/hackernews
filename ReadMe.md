# Hacker News
## News Android App with MVVM Architecture

API By https://github.com/HackerNews/API

## Overview
An android app list topstories from Hacker News API. Once the API fetched successfully it will saved into local db for future use.

Pages
- News list Page
- News details page
- Error screen

List of top stories will be displayed in news list page, user can search stories by id. When user clicks story id it will navigate to news detail page. In news details page when user click read article will redirect to web page for more info.
If the network is not connected and local db is also empty error screen will be displayed with retry option.

## Architecture
- MVVM - https://developer.android.com/jetpack/guide

## Dependencies
- Google Material
- Jetpack Viewmodel
- Jetpack Livedata
- Jetpack Room
- Jetpack Navigation
- Coroutines - Background
- Timber - Log
- SwipeRefresh
- Retrofit - Network call
- Koin - Dependency Injection

## Screenshot

![news_list.jpg](news_list.jpg)
![news_details.jpg](news_details.jpg)
![search.jpg](search_list.jpg)
![error_screen.jpg](error_screen.jpg)