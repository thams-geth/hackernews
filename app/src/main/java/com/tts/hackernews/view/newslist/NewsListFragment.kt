package com.tts.hackernews.view.newslist

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.tts.hackernews.R
import com.tts.hackernews.adapter.NewsListAdapter
import com.tts.hackernews.data.local.entity.NewsEntity
import com.tts.hackernews.data.repository.EventObserver
import com.tts.hackernews.databinding.FragmentNewsListBinding
import com.tts.hackernews.listeners.NewsClickListener
import com.tts.hackernews.utils.toast
import com.tts.hackernews.utils.visible
import com.tts.hackernews.viewmodel.NewsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class NewsListFragment : Fragment() {

    private var _binding: FragmentNewsListBinding? = null
    private val binding get() = _binding!!
    private val newsViewModel: NewsViewModel by viewModel()
    private var newsListAdapter: NewsListAdapter? = null

    private val newsClickListener = object : NewsClickListener {
        override fun onNewsClicked(position: Int, news: NewsEntity) {
            val bundle = bundleOf("id" to news.id)
            findNavController().navigate(R.id.newsDetailsFragment, bundle)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
//        newsViewModel = ViewModelProvider(this)[NewsViewModel::class.java]
        _binding = FragmentNewsListBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        setNewsAdapter()
        observeLiveData()
        getNewsData()
    }

    private fun getNewsData() {
        newsViewModel.getNews()
    }

    private fun observeLiveData() {
        with(newsViewModel) {
            loading.observe(viewLifecycleOwner, EventObserver {
                binding.swipeRefreshLayout.isRefreshing = it
            })

            serverError.observe(viewLifecycleOwner, {
                if (it != null) {
                    showErrorPage()
                }
            })

            newsData.observe(viewLifecycleOwner, {
                if (it != null && it.isNotEmpty()) {
                    newsListAdapter?.submitList(it)
                } else {
                    newsListAdapter?.submitList(listOf())
                }
            })
        }
    }

    private fun showErrorPage() {
        binding.rvNews.visible(false)
        binding.clError.visible(true)
    }

    private fun showSuccessPage() {
        binding.rvNews.visible(true)
        binding.clError.visible(false)
    }

    private fun setNewsAdapter() {
        newsListAdapter = NewsListAdapter(newsClickListener)
        binding.rvNews.adapter = newsListAdapter
    }


    private fun initViews() {
        binding.swipeRefreshLayout.setOnRefreshListener {
            showSuccessPage()
            getNewsData()
        }

        binding.btnRetry.setOnClickListener {
            showSuccessPage()
            getNewsData()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_news_list, menu)
        val searchItem: MenuItem = menu.findItem(R.id.action_search)
        val searchView = searchItem.actionView as SearchView
        searchView.queryHint = getString(R.string.search_news_by_id)
        searchView.inputType = InputType.TYPE_CLASS_NUMBER
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(search: String?): Boolean {
                if (search != null && search != "")
                    try {
                        val id = search.toLong()
                        newsViewModel.searchNewsById(id)
                    } catch (e: Exception) {
                        toast(requireContext(), getString(R.string.please_enter_valid_id))
                    }
                else {
                    newsViewModel.getNewsFromLocalDB()
                }
                return false
            }
        })

        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}