package com.tts.hackernews.view.newsdetails

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.tts.hackernews.data.repository.EventObserver
import com.tts.hackernews.databinding.FragmentNewsDetailsBinding
import com.tts.hackernews.utils.visible
import com.tts.hackernews.viewmodel.NewsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class NewsDetailsFragment : Fragment() {

    private var _binding: FragmentNewsDetailsBinding? = null
    private val binding get() = _binding!!
    private val newsViewModel: NewsViewModel by viewModel()
    private var id: Long? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.apply {
            id = getLong("id")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentNewsDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        observeLiveData()
        getNewsDetails()
    }

    private fun initViews() {
        binding.btnRetry.setOnClickListener {
            getNewsDetails()
        }
        binding.btnOpenURL.setOnClickListener {
            val url = binding.newsData?.url ?: return@setOnClickListener
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            if (i.resolveActivity(requireContext().packageManager) != null)
                startActivity(i)
        }
    }

    private fun getNewsDetails() {
        if (id != null) {
            newsViewModel.getNewsDetail(id ?: 0)
        }
    }

    private fun observeLiveData() {
        with(newsViewModel) {
            loading.observe(viewLifecycleOwner, EventObserver {
                binding.progressbar.visible(it)
            })
            newsDetailsData.observe(viewLifecycleOwner, {
                if (it != null) {
                    binding.newsData = it
                    showSuccessScreen()
                }
            })
            serverError.observe(viewLifecycleOwner, {
                if (it != null) {
                    showErrorScreen()
                }
            })
        }
    }

    private fun showErrorScreen() {
        binding.clNewsDetails.visible(false)
        binding.clError.visible(true)
    }

    private fun showSuccessScreen() {
        binding.clNewsDetails.visible(true)
        binding.clError.visible(false)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}