package com.tts.hackernews.di

import android.content.Context
import androidx.room.Room
import com.tts.hackernews.data.local.NewsDatabase
import com.tts.hackernews.data.local.dao.NewsDao

fun provideDatabase(context: Context): NewsDatabase {
    return Room.databaseBuilder(context, NewsDatabase::class.java, "news.db").build()
}

fun provideNewsDao(db: NewsDatabase): NewsDao {
    return db.newsDao()
}