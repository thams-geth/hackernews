package com.tts.hackernews.di

import com.tts.hackernews.data.remote.ApiInterceptor
import com.tts.hackernews.viewmodel.NewsViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val viewModelModule = module {
    viewModel { NewsViewModel(get()) }
}

val repositoryModule = module {
    single { provideNewsRepository(get(), get()) }
}

val databaseModule = module {
    single { provideDatabase(androidApplication()) }
    single { provideNewsDao(get()) }
}

val networkModule = module {
    single { ApiInterceptor() }
    single { provideLoggingInterceptor() }
    single { provideOkHttpClient(get(), get()) }
    single { provideRetrofit(get()) }
    single { provideApiService(get()) }
}