package com.tts.hackernews.di

import com.tts.hackernews.data.local.dao.NewsDao
import com.tts.hackernews.data.remote.ApiService
import com.tts.hackernews.data.repository.NewsRepository

fun provideNewsRepository(newsDao: NewsDao, apiService: ApiService): NewsRepository {
    return NewsRepository(newsDao, apiService)
}

