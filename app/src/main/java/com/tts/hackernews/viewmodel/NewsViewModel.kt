package com.tts.hackernews.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tts.hackernews.data.local.entity.NewsEntity
import com.tts.hackernews.data.repository.Event
import com.tts.hackernews.data.repository.NewsRepository
import com.tts.hackernews.data.repository.Results
import kotlinx.coroutines.launch

class NewsViewModel(private val newsRepository: NewsRepository) : ViewModel() {

    private val _serverError = MutableLiveData<String?>()
    val serverError: LiveData<String?>
        get() = _serverError

    private var _loading = MutableLiveData<Event<Boolean>>()
    val loading: LiveData<Event<Boolean>> get() = _loading

    private var _newsData = MutableLiveData<List<NewsEntity>?>()
    val newsData: LiveData<List<NewsEntity>?> get() = _newsData

    fun getNews() {
        viewModelScope.launch {
            _loading.value = Event(true)
            when (val result = newsRepository.getNews()) {
                is Results.Success -> {
                    _loading.value = Event(false)
                    saveNewsToLocalDB(result.value)
                }
                is Results.Failure -> {
                    _loading.value = Event(false)
                    getNewsFromLocalDB(result.statusResponse)
                }
            }
        }
    }

    fun getNewsFromLocalDB(statusResponse: String? = null) {
        viewModelScope.launch {
            if (newsRepository.getNewsFromLocalDB().isNotEmpty()) {
                _newsData.value = newsRepository.getNewsFromLocalDB()
            } else if (statusResponse != null) {
                _serverError.value = statusResponse
                _serverError.value = null
            }
        }
    }

    private suspend fun saveNewsToLocalDB(news: List<Long>) {
        _newsData.value = newsRepository.saveNewsLocally(news)
    }

    fun searchNewsById(id: Long) {
        viewModelScope.launch {
            _newsData.value = newsRepository.searchNewsById(id)
        }
    }

    private var _newsDetailsData = MutableLiveData<NewsEntity?>()
    val newsDetailsData: LiveData<NewsEntity?> get() = _newsDetailsData

    fun getNewsDetail(id: Long) {
        viewModelScope.launch {
            _loading.value = Event(true)
            when (val result = newsRepository.getNewsDetail(id)) {
                is Results.Success -> {
                    _loading.value = Event(false)
                    _newsDetailsData.value = result.value
                }
                is Results.Failure -> {
                    _loading.value = Event(false)
                    _serverError.value = result.statusResponse
                }
            }
        }
    }
}