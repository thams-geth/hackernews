package com.tts.hackernews.listeners

import com.tts.hackernews.data.local.entity.NewsEntity


interface NewsClickListener {
    fun onNewsClicked(position: Int, news: NewsEntity)
}
