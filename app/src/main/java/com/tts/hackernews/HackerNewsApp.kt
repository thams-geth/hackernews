package com.tts.hackernews

import android.app.Application
import com.tts.hackernews.di.databaseModule
import com.tts.hackernews.di.networkModule
import com.tts.hackernews.di.repositoryModule
import com.tts.hackernews.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class HackerNewsApp : Application() {
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        startKoin {
            androidContext(this@HackerNewsApp)
            modules(listOf(viewModelModule, repositoryModule, databaseModule, networkModule))
        }
    }
}