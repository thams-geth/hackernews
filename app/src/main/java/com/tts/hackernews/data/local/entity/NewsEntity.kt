package com.tts.hackernews.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale


@Entity(tableName = "news")
data class NewsEntity(
    @PrimaryKey
    var id: Long,
    val title: String? = null,
    val type: String? = null,
    val time: Long? = null,
    val score: Int? = null,
    val by: String? = null,
    val url: String? = null,
) {
    fun getDateString(): String? {
        val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
        return sdf.format(Date(time ?: 0))
    }
}