package com.tts.hackernews.data.remote

import okhttp3.Interceptor
import okhttp3.Response

class ApiInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val url = chain.request().url.newBuilder().addQueryParameter("print", "pretty").build()
        var newBuilder = chain.request().newBuilder()
        newBuilder = newBuilder.url(url)
        val request = newBuilder.build()
        return chain.proceed(request)
    }
}