package com.tts.hackernews.data.remote

import com.tts.hackernews.data.local.entity.NewsEntity
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @GET(allNewsURL)
    suspend fun getNews(): List<Long>

    @GET(newsDetailsURL)
    suspend fun getNewsDetail(@Path("id") newsId: Long): NewsEntity

    companion object {
        const val allNewsURL = "/v0/topstories.json"
        const val newsDetailsURL = "/v0/item/{id}.json"
    }
}