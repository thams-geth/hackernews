package com.tts.hackernews.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.tts.hackernews.data.local.entity.NewsEntity

@Dao
interface NewsDao {

    @Insert
    suspend fun insertAllNews(news: List<NewsEntity>)

    @Query("SELECT * FROM news")
    suspend fun getAllNews(): List<NewsEntity>

    // end with and start with
    @Query("SELECT * FROM news WHERE id LIKE '%' || :newsId || '%' ")
    suspend fun searchNewsById(newsId: Long): List<NewsEntity>

    @Query("DELETE FROM news")
    suspend fun deleteAllNews()

}