package com.tts.hackernews.data.repository

import com.tts.hackernews.data.local.dao.NewsDao
import com.tts.hackernews.data.local.entity.NewsEntity
import com.tts.hackernews.data.remote.ApiService

class NewsRepository(private val newsDao: NewsDao, private val apiService: ApiService) : BaseRepository() {

    suspend fun getNews() = safeApiCall { apiService.getNews() }

    suspend fun getNewsDetail(id: Long) = safeApiCall { apiService.getNewsDetail(id) }

    suspend fun saveNewsLocally(news: List<Long>): List<NewsEntity> {
        deleteNews()
        val data = news.map { value -> NewsEntity(id = value) }
        newsDao.insertAllNews(data)
        return getNewsFromLocalDB()
    }

    suspend fun getNewsFromLocalDB(): List<NewsEntity> {
        return newsDao.getAllNews()
    }

    suspend fun searchNewsById(id: Long): List<NewsEntity> {
        return newsDao.searchNewsById(id)
    }

    private suspend fun deleteNews() {
        newsDao.deleteAllNews()
    }
}