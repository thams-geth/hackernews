package com.tts.hackernews.data.repository

import android.accounts.NetworkErrorException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import timber.log.Timber
import java.net.UnknownHostException

open class BaseRepository {

    suspend fun <T> safeApiCall(
        apiCall: suspend () -> T
    ): Results<T> {
        return withContext(Dispatchers.IO) {
            try {
                Results.Success(apiCall.invoke())
            } catch (e: Exception) {
                Timber.e(e.message.toString())
                when (e) {
                    is HttpException -> {
                        val errorBody = e.response()?.errorBody()?.charStream()?.readText()
                        Timber.e("errorBody -- $errorBody")
                        Results.Failure(e.code(), ErrorStatus.HTTP_EXCEPTION, errorBody ?: "")
                    }
                    is NetworkErrorException -> {
                        val errorMessage = e.message ?: ""
                        Timber.e("errorMessage -- $errorMessage")
                        Results.Failure(0, ErrorStatus.NO_CONNECTION, errorMessage)
                    }
                    is UnknownHostException -> {
                        val errorMessage = e.message ?: ""
                        Timber.e("errorMessage -- $errorMessage")
                        Results.Failure(0, ErrorStatus.NO_CONNECTION, errorMessage)
                    }
                    else -> {
                        val errorMessage = e.message ?: ""
                        Timber.e("errorMessage -- $errorMessage")
                        Results.Failure(0, ErrorStatus.UNKNOWN, errorMessage)
                    }
                }
            }
        }
    }
}
