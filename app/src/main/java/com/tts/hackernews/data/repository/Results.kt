package com.tts.hackernews.data.repository

sealed class Results<out T> {
    data class Success<out T>(val value: T) : Results<T>()
    data class Failure(
        val statusCode: Int,
        val errorStatus: ErrorStatus,
        val statusResponse: String = ""
    ) : Results<Nothing>()
}

enum class ErrorStatus {
    HTTP_EXCEPTION,
    NO_CONNECTION,
    UNKNOWN,
}