package com.tts.hackernews.utils

object Constants {
    const val DOMAIN = "https://hacker-news.firebaseio.com"
    const val TIME_OUT = 60L
}