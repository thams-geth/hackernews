package com.tts.hackernews.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.tts.hackernews.data.local.entity.NewsEntity
import com.tts.hackernews.databinding.ItemNewsBinding
import com.tts.hackernews.listeners.NewsClickListener

class NewsListAdapter(private val newsClickListener: NewsClickListener) :
    ListAdapter<NewsEntity, NewsViewHolder>(NewsDiffUtils) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        return NewsViewHolder(
            ItemNewsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.setNote(getItem(position), newsClickListener)
    }

    object NewsDiffUtils : DiffUtil.ItemCallback<NewsEntity>() {
        override fun areItemsTheSame(oldItem: NewsEntity, newItem: NewsEntity): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: NewsEntity, newItem: NewsEntity): Boolean {
            return oldItem == newItem
        }

    }
}

class NewsViewHolder(private val view: ItemNewsBinding) : RecyclerView.ViewHolder(view.root) {
    fun setNote(news: NewsEntity, newsClickListener: NewsClickListener) {
        view.newsData = news
        view.root.setOnClickListener {
            newsClickListener.onNewsClicked(position = adapterPosition, news = news)
        }
    }
}

